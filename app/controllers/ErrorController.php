<?php namespace App\Controllers;

use \Framework\Controller;

class ErrorController extends Controller
{
    public function error($code)
    {
        echo "ERROR: $code";
    }
}