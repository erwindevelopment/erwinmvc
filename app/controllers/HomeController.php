<?php namespace App\Controllers;

use \Framework\Controller;
use \Framework\Model;

class HomeController extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $this->view('index', [
            'message' => 'Welcome to the Erwin Tiny MVC'
        ]);
    }

    public function about()
    {
        echo "this is about us";
    }
}
