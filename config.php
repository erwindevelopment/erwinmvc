<?php

//LOCATIONS
define('base', __DIR__ . '/');
define('app', base . 'app/');
define('controllers', app . 'controllers/');
define('models', app . 'models/');
define('views', app . 'views/');
define('database', base . 'database/');
define('storage', base . 'storage/');

//DATABASE
define('DBDRIVER', 'sqlite');
define('DBHOST', '');
define('DBNAME', database . 'database.sqlite');
define('DBLOGIN', '');
define('DBPASS', '');
define('DBCHARSET', 'utf8');
define('COLLATION', 'utf8_unicode_ci');

//ROUTES
require 'routes.php';
