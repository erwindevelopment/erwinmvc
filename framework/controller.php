<?php namespace Framework;

use Twig_Loader_Filesystem as Loader;
use Twig_Environment as Environment;
use Twig_Lexer as Lexer;

abstract class Controller
{
    private $twig;

    public function __construct()
    {
        $this->twig = new Environment(new Loader(views), [
            'cache' => storage . 'cache',
            'debug' => true
        ]);

        $this->twig->setLexer(new Lexer($this->twig, [
            'tag_comment'   => ['[#', '#]'],
            'tag_block'     => ['[', ']'],
            'tag_variable'  => ['[[', ']]'],
            'interpolation' => ['#[', ']']
        ]));
    }

    protected function view($view, $params = [])
    {
        echo $this->twig->render("$view.html", $params);
    }
}
