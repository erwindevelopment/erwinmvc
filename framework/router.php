<?php namespace Framework;

class Router
{
    private $routes = [];

    public function addRoute($route, $destination)
    {
        $this->routes[$route] = $destination;
    }

    public function loadRoute()
    {
        $routes = $this->routes;
        $command = isset($_SERVER['REQUEST_URI']) ? $_SERVER['REQUEST_URI'] : "/";

        if(array_key_exists($command, $routes))
        {
            $command = explode('@', $routes[$command]);

            $controller = $command[0];
            $action = $command[1];

            require_once(controllers . "$controller.php");

            $class = "\\App\\Controllers\\$controller";
            $controller = new $class;

            return $controller->$action();
        }
        else
        {
            $obj = new \App\Controllers\ErrorController;

            return $obj->error('404');
        }
    }
}
