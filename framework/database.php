<?php namespace Framework;

use Illuminate\Database\Capsule\Manager as DBManager;

class Database
{
    function __construct()
    {
        $db = new DBManager;

        $db->addConnection([
            'driver'        => DBDRIVER,
            //'host'        => DBHOST,
            'database'      => DBNAME,
            //'username'    => DBLOGIN,
            //'password'    => DBPASS,
            'charset'       => DBCHARSET,
            'collation'     => COLLATION
        ]);

        $db->bootEloquent();
    }
}