<?php

use \Framework\Database as Eloquent;

require __DIR__ . '/../vendor/autoload.php';
require __DIR__ . '/../config.php';

new Eloquent;

$router->loadRoute();
