const mix = require('laravel-mix');

mix.js('src/js/main.js', 'public_html/js')
   .sass('src/sass/styles.scss', 'public_html/css');
