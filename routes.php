<?php

use Framework\Router;

$router = new Router;

$router->addRoute('/', 'HomeController@index');
$router->addRoute('/about', 'HomeController@about');
